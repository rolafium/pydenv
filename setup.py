import os
from setuptools import find_packages, setup
from pydenv.version import VERSION


with open(os.path.join(os.path.dirname(__file__), "README.md")) as fp:
    README = fp.read()


setup(
    name="pydenv",
    version=VERSION,
    packages=find_packages(),
    include_package_data=True,
    license="Apache-2.0",
    description="Python utilities extending built-ins (i.e. Exceptions) and easing development/reporting",
    long_description=README,
    url="https://gitlab.com/rolafium/pydenv",
    author="William Di Pasquale",
    author_email="rolafium@protonmail.com",
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache-2.0',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
