"""
Tests the PyDenvObject
module: pydenv.conf
"""
import pytest
from pydenv.conf import PyDenvObject


@pytest.fixture(autouse=True)
def reset_is_debug():
    """Rests is_debug before each test"""
    PyDenvObject.is_debug = PyDenvObject.__isdebug__


class TestPyDenvObject(object):
    """Tests PyDenvObject"""

    def test_default_is_debug(self):
        """Tests the default response is_debug is False"""
        assert PyDenvObject.is_debug() == False

    def test_set_is_debug_func(self):
        """Tests set_is_debug_func"""
        def func_true(pydenv):
            """Test func, returns True"""
            return True

        def func_false(pydenv):
            """Test func, returns False"""
            return False

        PyDenvObject.set_is_debug_func(func_true)
        assert PyDenvObject.is_debug.__name__ == func_true.__name__
        assert PyDenvObject.is_debug() == True

        PyDenvObject.set_is_debug_func(func_false)
        assert PyDenvObject.is_debug.__name__ == func_false.__name__
        assert PyDenvObject.is_debug() == False

    def test_activate_debug(self):
        """Tests the activate debug function"""
        PyDenvObject.is_debug = None
        PyDenvObject.activate_debug()

        assert PyDenvObject.is_debug() == True

    def test_deactivate_debug(self):
        """Tests the activate debug function"""
        PyDenvObject.is_debug = None
        PyDenvObject.deactivate_debug()

        assert PyDenvObject.is_debug() == False
