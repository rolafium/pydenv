"""
Tests the PyDenv output utils
module: pydenv.output
"""
from pydenv.output import StylesCodes, ForegroundColorsCodes, BackgroundColorsCodes, TextStyles


def test_style_codes_attrs():
    """Checks StylesCodes has all the expected attrs"""
    expected_attrs = [
        "DEFAULT",
        "BOLD",
        "FAINT",
        "ITALIC",
        "UNDERLINE",
    ]

    for attr in expected_attrs:
        assert hasattr(StylesCodes, attr)

def test_foreground_colors_codes_attrs():
    """Checks ForegroundColorsCodes has all the expected attrs"""
    expected_attrs = [
        "BLACK",
        "RED",
        "GREEN",
        "YELLOW",
        "BRIGHT_BLACK",
        "WHITE",
    ]

    for attr in expected_attrs:
        assert hasattr(ForegroundColorsCodes, attr)

def test_background_colors_codes_attrs():
    """Checks BackgroundColorsCodes has all the expected attrs"""
    expected_attrs = [
        "BLACK",
        "RED",
        "GREEN",
        "YELLOW",
        "BRIGHT_BLACK",
        "WHITE",
    ]

    for attr in expected_attrs:
        assert hasattr(BackgroundColorsCodes, attr)

def test_text_styles_attrs():
    """Checks TextStyles has all the expected attrs"""
    expected_attrs = [
        "CODE_TEMPLATE",
        "DEFAULT_FORMATTED",
        "styles",
        "colors",
    ]

    for attr in expected_attrs:
        assert hasattr(TextStyles, attr)

def test_text_styles_format_no_styles():
    """Checks StylesCodes returns the original text with no styles"""
    styles = []
    output = TextStyles.format_text("Hello", styles)
    assert "Hello" == output

def test_text_styles_format_text_one_style():
    """Checks StylesCodes correctly formats the text with one style"""
    styles = ["d"]
    output = TextStyles.format_text("Hello2", styles)
    assert "Hello2" in output
    assert TextStyles.DEFAULT_FORMATTED in output
    assert ";".join(styles) in output

def test_text_styles_format_text_three_styles():
    """Checks StylesCodes correctly formats the text with three styles"""
    styles = ["a", "b", "c"]
    output = TextStyles.format_text("Hello1", styles)
    assert "Hello1" in output
    assert TextStyles.DEFAULT_FORMATTED in output
    assert ";".join(styles) in output
