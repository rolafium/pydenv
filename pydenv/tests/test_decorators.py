"""
Tests for the PyDenv decorators
module: pydenv.decorators
"""
from unittest.mock import MagicMock
from pydenv.decorators import cached_property


def test_cached_property():
    """Tests the property is internally cached"""
    my_func = MagicMock()
    my_func.return_value = "CACHED_PROPERTY"

    class TestObject(object):
        @cached_property
        def my_property(self):
            return my_func()
    
    obj = TestObject()
    assert not hasattr(obj, "_pydenv_cached_my_property")

    val = obj.my_property
    assert hasattr(obj, "_pydenv_cached_my_property")
    assert getattr(obj, "_pydenv_cached_my_property") == my_func.return_value
    assert val == my_func.return_value
    assert 1 == my_func.call_count

    val_again = obj.my_property
    assert val_again == my_func.return_value
    assert 1 == my_func.call_count
