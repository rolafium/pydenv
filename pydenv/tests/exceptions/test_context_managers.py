"""
Tests the exception package context managers
module: pydenv.exceptions.context_managers
"""
from unittest.mock import patch
from pydenv.exceptions.context_managers import TempTracebackLimit


@patch("pydenv.exceptions.context_managers.sys")
def test_init_method(sys_mock):
    sys_mock.tracebacklimit = "TRACEBACK_LIMIT"
    ttl1 = TempTracebackLimit()
    ttl2 = TempTracebackLimit(temp_limit=999)

    assert ttl1.temp_limit == 10
    assert ttl2.temp_limit == 999
    assert ttl1.predefined_limit == sys_mock.tracebacklimit
    assert ttl2.predefined_limit == sys_mock.tracebacklimit

@patch("pydenv.exceptions.context_managers.sys")
def test_enter_method(sys_mock):
    sys_mock.tracebacklimit = "TRACEBACK_LIMIT"
    ttl = TempTracebackLimit()

    ttl.__enter__()
    assert sys_mock.tracebacklimit == ttl.temp_limit

@patch("pydenv.exceptions.context_managers.sys")
def test_exit_method(sys_mock):
    sys_mock.tracebacklimit = "TRACEBACK_LIMIT"
    ttl = TempTracebackLimit()
    sys_mock.tracebacklimit = "ANOTHER_VALUE"

    ttl.__exit__()
    assert sys_mock.tracebacklimit == "TRACEBACK_LIMIT"

@patch("pydenv.exceptions.context_managers.sys")
def test_as_context_manager(sys_mock):
    sys_mock.tracebacklimit = "TRACEBACK_LIMIT"

    with TempTracebackLimit(temp_limit=999):
        assert sys_mock.tracebacklimit == 999

    assert sys_mock.tracebacklimit == "TRACEBACK_LIMIT"
