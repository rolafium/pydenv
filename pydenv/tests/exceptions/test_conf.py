"""
Tests the configuration for the PyDenv exceptions
module: pydenv.exceptions.conf
"""
from pydenv.exceptions.conf import PyDenvExceptionConfig


def test_conf_has_required_attrs():
    """Checks if all the required attrs are defined"""
    expected_attrs = [
        "PYDENV_EXCEPTION_TEMPLATE",
        "PYDENV_TRACEBACK_TEMPLATE",
        "PYDENV_EXCEPTION_PRINT_FUNCTIONS",
        "PYDENV_EXCEPTION_TAGS",
    ]

    for attr in expected_attrs:
        assert hasattr(PyDenvExceptionConfig, attr)

def test_conf_default_print_funcs():
    """Checks the default exception print functions"""
    print_funcs = PyDenvExceptionConfig.PYDENV_EXCEPTION_PRINT_FUNCTIONS
    assert "print" in print_funcs

    pf_def = print_funcs["print"]
    assert pf_def["func"] == print
    assert pf_def["apply_tags"]
    assert pf_def["apply_styles"]

def test_conf_default_tags():
    """Checks the default exception tags"""
    tags = PyDenvExceptionConfig.PYDENV_EXCEPTION_TAGS
    assert "venv" in tags
    assert "base_dir" in tags
