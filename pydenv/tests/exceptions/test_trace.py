"""
Tests for the PyDenvTrace object
module: pydenv.exceptions.trace
"""
import pytest
from unittest.mock import patch
from traceback import extract_stack
from pydenv.output import TextStyles
from pydenv.exceptions.internal import PyDenvImproperlyConfigured
from pydenv.exceptions.trace import PyDenvTrace


def test_init():
    """Tests the attributes of the object"""
    traces = extract_stack()
    pdt = PyDenvTrace(traces[0])

    assert traces[0] == pdt.trace

def test_template_property():
    """Checks if the template is correctly generated"""
    trace = extract_stack()[0]
    pdt = PyDenvTrace(trace)
    template = pdt.template

    assert pdt.trace.filename in template
    assert str(pdt.trace.lineno) in template
    assert pdt.trace.name in template
    assert pdt.trace.line in template

def test_spec_no_template():
    """Raises an PyDenvImproperlyConfigured exception when template is None"""
    trace = extract_stack()[0]
    pdt = PyDenvTrace(trace)
    pdt._pydenv_cached_template = None

    with pytest.raises(PyDenvImproperlyConfigured) as e:
        spec = pdt.spec

def test_spec_with_template():
    """Reads the tags correctly"""
    trace = extract_stack()[0]
    pdt = PyDenvTrace(trace)

    template = ""

    for key in pdt.PYDENV_EXCEPTION_TAGS:
        tag = pdt.PYDENV_EXCEPTION_TAGS[key]
        template += tag.get("value") or ""

    pdt._pydenv_cached_template = template
    spec = pdt.spec

    for key in pdt.PYDENV_EXCEPTION_TAGS:
        tag = pdt.PYDENV_EXCEPTION_TAGS[key]
        style = tag.get("style", TextStyles.colors.foreground.BRIGHT_BLACK)
        
        if tag.get("value"):
            assert tag in spec["tags"]
            assert style in spec["styles"]
        else:
            assert not tag in spec["tags"]

def test_format_no_apply():
    """Checks if template is returned when no apply is executed"""
    trace = extract_stack()[0]
    pdt = PyDenvTrace(trace)
    pdt._pydenv_cached_template = "/tag1/tag2/content"

    assert pdt.format() == pdt.template

def test_format_apply_tags():
    """Checks if the tags are applied"""
    trace = extract_stack()[0]
    pdt = PyDenvTrace(trace)
    pdt._pydenv_cached_template = "/tag1/tag2/content"
    pdt._pydenv_cached_spec = {
        "tags": [
            { "value": "tag1", "repl": "[tag1]" },
            { "value": "tag2", "repl": "[tag2]" },
        ]
    }

    formatted = pdt.format(apply_tags=True)
    
    assert formatted == "/[tag1]/[tag2]/content"

def test_format_apply_styles():
    """Checks if the styles are applied"""
    trace = extract_stack()[0]
    pdt = PyDenvTrace(trace)

    pdt._pydenv_cached_template = "/tag1/tag2/content"
    pdt._pydenv_cached_spec = { "styles": ["A", "B", "C"] }
    expected_styled = TextStyles.format_text(pdt.template, pdt.spec["styles"])
    styled = pdt.format(apply_styles=True)

    assert expected_styled == styled

@patch("pydenv.exceptions.trace.PyDenvTrace.format")
def test_str_method(format_mock):
    """Checks if the str method is valid"""
    trace = extract_stack()[0]
    pdt = PyDenvTrace(trace)
    expected_str = "PYDENVTRACE_STRING"
    format_mock.return_value = expected_str

    assert str(pdt) == expected_str
    assert 1 == format_mock.call_count

def test_repr_method():
    """Checks if the repr method is valid"""
    trace = extract_stack()[0]
    pdt = PyDenvTrace(trace)

    pdt_repr = pdt.__repr__()
    assert pdt.__class__.__name__ in pdt_repr
    assert str(pdt.trace) in pdt_repr
