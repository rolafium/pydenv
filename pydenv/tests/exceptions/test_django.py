"""
Tests for the PyDenv Django exception utils
module: pydenv.exception.django
"""
from unittest.mock import MagicMock, patch
from pydenv.exceptions.django import PyDenvExceptionMiddleware


def test_pydenv_middleware_init():
    """Checks the middleware inits correctly"""
    get_response_mock = "GET_RESPONSE"

    pem = PyDenvExceptionMiddleware(get_response_mock)
    assert pem.get_response == get_response_mock

def test_pydenv_middleware_call():
    """Checks the middleware is called and does nothing to the response object"""
    expected_data = "RESPONSE"
    request = "REQUEST"
    get_response_mock = MagicMock()
    get_response_mock.return_value = expected_data

    pem = PyDenvExceptionMiddleware(get_response_mock)
    response = pem(request)
    assert expected_data == response
    assert 1 == get_response_mock.call_count
    assert request == get_response_mock.call_args[0][0]

@patch("pydenv.exceptions.django.pydenv_report_exception")
@patch("pydenv.exceptions.django.pydenv_extract_tb")
def test_pydenv_process_exception(extract_mock, report_mock):
    """Checks the process_exception call reports the exception as expected"""
    tb = "TB"
    extract_mock.return_value = tb
    get_response_mock = "GET_RESPONSE"
    request = "REQUEST"
    exception = "EXCEPTION"

    pem = PyDenvExceptionMiddleware(get_response_mock)
    handled_return_value = pem.process_exception(request, exception) # pylint: disable=E1128
    assert None == handled_return_value
    assert 1 == extract_mock.call_count
    assert 1 == report_mock.call_count
    assert exception == extract_mock.call_args[0][0]
    assert exception == report_mock.call_args[0][0]
    assert tb == report_mock.call_args[0][1]
