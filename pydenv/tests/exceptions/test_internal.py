"""
Tests the internal PyDenv exceptions
module: pydenv.excetions.internal
"""
from pydenv.exceptions.internal import PyDenvException, PyDenvInvalidTraceback, PyDenvImproperlyConfigured


def test_pydenv_exception_type():
    """Checks if PyDenvExceptions is subclass of Exception"""
    assert issubclass(PyDenvException, Exception)

def test_pydenv_invalid_traceback_type():
    """Checks if PyDenvImproperlyConfigured is subclass of PyDenvException"""
    assert issubclass(PyDenvInvalidTraceback, PyDenvException)

def test_pydenv_improperly_configured_type():
    """Checks if PyDenvImproperlyConfigured is subclass of PyDenvException"""
    assert issubclass(PyDenvImproperlyConfigured, PyDenvException)
