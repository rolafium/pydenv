"""
Tests the functions that manage exceptions
module: pydenv.exceptions.functions
"""
from unittest.mock import MagicMock, patch
from pydenv.exceptions.functions import (
    pydenv_extract_tb,
    pydenv_get_trace_list,
    pydenv_report_exception,
    pydenv_exception_handler,
    set_pydenv_exception_handler,
    extend_with_pydenv_exception_handler,
)


@patch("pydenv.exceptions.functions.extract_stack")
def test_extract_tb_no_param(extract_stack_mock):
    """Checks if the stacktrace is extracted when there is no param"""
    extract_stack_mock.return_value = ["a", "b", "c"]
    output = pydenv_extract_tb()
    assert ["a", "b"] == output
    assert 1 == extract_stack_mock.call_count

@patch("pydenv.exceptions.functions.extract_stack")
def test_extract_tb_obj_is_none(extract_stack_mock):
    """Checks if the stacktrace is extracted when the param is none"""
    extract_stack_mock.return_value = ["a", "b", "c"]
    output = pydenv_extract_tb(None)
    assert ["a", "b"] == output
    assert 1 == extract_stack_mock.call_count

@patch("pydenv.exceptions.functions.extract_stack")
def test_extract_tb_obj_no_traceback(extract_stack_mock):
    """Checks if the stacktrace is extracted when the param has no attr __traceback__"""
    extract_stack_mock.return_value = ["a", "b", "c"]
    obj = object()
    output = pydenv_extract_tb(obj)
    assert ["a", "b"] == output
    assert 1 == extract_stack_mock.call_count

@patch("pydenv.exceptions.functions.extract_stack")
def test_extract_tb_obj_traceback_is_none(extract_stack_mock):
    """Checks if the stacktrace is extracted when the param __traceback__ attr is None"""
    extract_stack_mock.return_value = ["a", "b", "c"]
    obj = MagicMock()
    obj.__traceback__ = None
    output = pydenv_extract_tb(obj)
    assert ["a", "b"] == output
    assert 1 == extract_stack_mock.call_count

@patch("pydenv.exceptions.functions.extract_tb")
@patch("pydenv.exceptions.functions.extract_stack")
def test_extract_tb_obj_traceback_raises_attribute_error(extract_stack_mock, extract_tb_mock):
    """Checks if the stacktrace is extracted when the param __traceback__ attr is None"""
    extract_tb_mock.side_effect = AttributeError
    extract_stack_mock.return_value = ["a", "b", "c"]
    obj = MagicMock()
    obj.__traceback__ = "TRACEBACK"
    output = pydenv_extract_tb(obj)
    assert ["a", "b"] == output
    assert 1 == extract_stack_mock.call_count

@patch("pydenv.exceptions.functions.extract_tb")
def test_extract_tb_obj_traceback_is_not_none(extract_tb_mock):
    """Checks if the traceback is extracted when the param  __traceback__ is an object"""
    extract_tb_mock.return_value = ["a", "b"]
    obj = MagicMock()
    obj.__traceback__ = "TRACEBACK"
    output = pydenv_extract_tb(obj)
    assert ["a", "b"] == output
    assert 1 == extract_tb_mock.call_count
    assert obj.__traceback__ == extract_tb_mock.call_args[0][0]

@patch("pydenv.exceptions.functions.PyDenvTrace")
def test_get_trace_list_empty_list(trace_mock):
    """Checks if the trace list is retrieved correctly from an empty list"""
    traces = pydenv_get_trace_list([])
    assert [] == traces

@patch("pydenv.exceptions.functions.PyDenvTrace")
def test_get_trace_list_with_list(trace_mock):
    """Checks if the trace list is retrieved correctly from a non empty list"""
    tb = ["a", "b", "c"]
    traces = pydenv_get_trace_list(tb)

    assert 3 == len(traces)
    assert 3 == trace_mock.call_count
    assert tb[0] == trace_mock.call_args_list[0][0][0]
    assert tb[1] == trace_mock.call_args_list[1][0][0]
    assert tb[2] == trace_mock.call_args_list[2][0][0]

@patch("pydenv.exceptions.functions.pydenv_get_trace_list")
@patch("pydenv.exceptions.functions.PyDenvExceptionConfig.PYDENV_EXCEPTION_TEMPLATE")
@patch("pydenv.exceptions.functions.PyDenvExceptionConfig.PYDENV_EXCEPTION_PRINT_FUNCTIONS")
def test_report_exception_no_print_funcs(print_funcs_mock, template_mock, get_trace_mock):
    """Checks if it reports without any print functions"""
    print_funcs_mock.values.return_value = []
    get_trace_mock.return_value = ["a", "b", "c"]

    pydenv_report_exception(MagicMock(), ["a", "b", "c"])
    assert 0 == template_mock.call_count

@patch("pydenv.exceptions.functions.pydenv_get_trace_list")
@patch("pydenv.exceptions.functions.PyDenvExceptionConfig.PYDENV_EXCEPTION_TEMPLATE")
@patch("pydenv.exceptions.functions.PyDenvExceptionConfig.PYDENV_EXCEPTION_PRINT_FUNCTIONS")
def test_report_exception_default_print_funcs(print_funcs_mock, template_mock, get_trace_mock):
    """Checks if it reports with both the defined print functions"""
    print_func_args_1 = { "apply_tags": False, "apply_styles": False }
    print_func_args_2 = { "apply_tags": True, "apply_styles": True }

    print_func_1 = MagicMock()
    print_func_2 = MagicMock()

    print_funcs_mock.values.return_value = [
        dict({ "func": print_func_1 }, **print_func_args_1),
        dict({ "func": print_func_2 }, **print_func_args_2),
    ]

    trace = MagicMock()
    trace.format.return_value = "ABC"
    get_trace_mock.return_value = [ trace, trace, trace ]

    template_mock.format.return_value = "TEMPLATE_MOCK"

    pydenv_report_exception(MagicMock(), ["a", "b", "c"])
    assert 2 == template_mock.format.call_count
    assert 6 == trace.format.call_count

    assert print_func_args_1 == trace.format.call_args_list[0][1]
    assert print_func_args_1 == trace.format.call_args_list[1][1]
    assert print_func_args_1 == trace.format.call_args_list[2][1]
    assert print_func_args_2 == trace.format.call_args_list[3][1]
    assert print_func_args_2 == trace.format.call_args_list[4][1]
    assert print_func_args_2 == trace.format.call_args_list[5][1]

    assert 1 == print_func_1.call_count
    assert 1 == print_func_2.call_count
    assert template_mock.format.return_value == print_func_1.call_args[0][0]
    assert template_mock.format.return_value == print_func_2.call_args[0][0]

@patch("pydenv.exceptions.functions.extract_tb")
@patch("pydenv.exceptions.functions.pydenv_report_exception")
def test_exception_handler(report_mock, extract_mock):
    """Checks if the exception handler correctly reports when called"""
    extract_mock.return_value = "TB"

    call_args = ("CLASS", "INSTANCE", "TRACEBACK",)
    pydenv_exception_handler(*call_args)

    assert 1 == extract_mock.call_count
    assert call_args[2] == extract_mock.call_args[0][0]

    assert 1 == report_mock.call_count
    assert call_args[1] == report_mock.call_args[0][0]
    assert extract_mock.return_value == report_mock.call_args[0][1]


@patch("pydenv.exceptions.functions.sys")
def test_set_exception_handler(sys_mock):
    """Checks if the sys.excepthook is properly set"""
    set_pydenv_exception_handler()
    assert pydenv_exception_handler == sys_mock.excepthook

@patch("pydenv.exceptions.functions.pydenv_exception_handler")
@patch("pydenv.exceptions.functions.sys")
def test_extend_exception_handler_no_func(sys_mock, handler_mock):
    """Checks if the sys.excepthook is extended with the pydenv handler"""
    sys_mock.__excepthook__ = MagicMock()
    extend_with_pydenv_exception_handler(None)

    assert 0 == sys_mock.__excepthook__.call_count
    assert 0 == handler_mock.call_count

    call_args = ("CLASS", "INSTANCE", "TRACEBACK",)
    sys_mock.excepthook(*call_args)

    assert 1 == sys_mock.__excepthook__.call_count
    assert 1 == handler_mock.call_count
    assert call_args == sys_mock.__excepthook__.call_args[0]
    assert call_args == handler_mock.call_args[0]

@patch("pydenv.exceptions.functions.pydenv_exception_handler")
@patch("pydenv.exceptions.functions.sys")
def test_extend_exception_handler_with_func(sys_mock, handler_mock):
    """Checks if the sys.excepthook is extended with the pydenv handler and a third function"""
    sys_mock.__excepthook__ = MagicMock()
    additional_func = MagicMock()

    extend_with_pydenv_exception_handler(additional_func)

    assert 0 == sys_mock.__excepthook__.call_count
    assert 0 == handler_mock.call_count
    assert 0 == additional_func.call_count

    call_args = ("CLASS", "INSTANCE", "TRACEBACK",)
    sys_mock.excepthook(*call_args)

    assert 1 == sys_mock.__excepthook__.call_count
    assert 1 == handler_mock.call_count
    assert 1 == additional_func.call_count
    assert call_args == sys_mock.__excepthook__.call_args[0]
    assert call_args == handler_mock.call_args[0]
    assert call_args == additional_func.call_args[0]
