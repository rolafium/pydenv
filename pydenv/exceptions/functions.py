"""
Functions to handle exceptions
"""
import os
import sys
from traceback import extract_tb, extract_stack
from pydenv.exceptions.conf import PyDenvExceptionConfig
from pydenv.exceptions.trace import PyDenvTrace
from pydenv.exceptions.internal import PyDenvInvalidTraceback


def pydenv_extract_tb(obj=None):
    """
    Gets the trace list = outputs of extract_tb or extract_stack
    If no object is supplied, extract_stack is called
    :param obj: object/None
    :returns: List of traces
    """
    try:
        if not obj:
            raise PyDenvInvalidTraceback
        if not hasattr(obj, "__traceback__"):
            raise PyDenvInvalidTraceback
        if obj.__traceback__ is None:
            raise PyDenvInvalidTraceback
        # We can now try to extract the tb from obj.__traceback__
        return extract_tb(obj.__traceback__)
    except AttributeError:
        # If for any reason obj.__traceback__ is neither
        # a valid traceback object or None
        # we need to catch the upcoming AttributeError
        # and resort to the default behaviour
        # of return the stack rather than the traceback
        return extract_stack()[:-1]
    except PyDenvInvalidTraceback:
        # The supplied object is either None or invalid
        # so we resort to return the stack
        # removing the last element, which is the call to this function
        return extract_stack()[:-1]

def pydenv_get_trace_list(tb):
    """
    Returns a list of PyDenvTrace
    :param tb: output of extract_tb or extract_stack
    :returns: List<PyDenvTrace>
    """
    traces = [ PyDenvTrace(trace) for trace in tb ]
    return traces


def pydenv_report_exception(instance, tb):
    """
    Reports an exception
    :param instance: Exception instance
    :param tb: output of extract_tb or extract_stack
    """
    traces = pydenv_get_trace_list(tb)

    for print_func in PyDenvExceptionConfig.PYDENV_EXCEPTION_PRINT_FUNCTIONS.values():
        func = print_func["func"]
        apply_tags = print_func.get("apply_tags", False)
        apply_styles = print_func.get("apply_styles", False)

        formatted_traces = [
            trace.format(apply_tags=apply_tags, apply_styles=apply_styles)
            for trace in traces
        ]

        output = PyDenvExceptionConfig.PYDENV_EXCEPTION_TEMPLATE.format(
            exc_class=instance.__class__.__name__,
            instance=instance,
            traces="\n".join(formatted_traces),
        )

        func(output)

def pydenv_exception_handler(exc_class, instance, traceback):
    """
    Default pydenv exception handler to be supplied to sys.excepthook
    :param exc_class:
    :param instance:
    :param traceback:
    """
    trace_list = extract_tb(traceback)
    pydenv_report_exception(instance, trace_list)


def set_pydenv_exception_handler():
    """
    Sets pydenv_exception_handler as sys.exceptook
    """
    sys.excepthook = pydenv_exception_handler

def extend_with_pydenv_exception_handler(func=None):
    """
    Extends the default sys.__excepthook__ with pydenv and a supplied func
    :param func: Optional user defined exception handler to be executed after pydenv
    """ 
    def extended_hook(exc_class, instance, traceback):
        """
        Defines a combined exception hook
        :param exc_class:
        :param instance:
        :param traceback:
        """
        pydenv_exception_handler(exc_class, instance, traceback)

        if func is not None:
            func(exc_class, instance, traceback)

        sys.__excepthook__(exc_class, instance, traceback)
    
    sys.excepthook = extended_hook
