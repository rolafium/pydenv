"""
Context managers for the pydenv.exceptions package
"""
import sys


class TempTracebackLimit(object):
    """
    Context manager that alters the sys.tracebacklimit attribute
    to the speficied temp_limit value
    """

    def __init__(self, temp_limit=10):
        """
        Inits the context manager
        :param temp_limit: number of requested traceback lines
        """
        self.temp_limit = temp_limit
        self.predefined_limit = sys.tracebacklimit

    def __enter__(self):
        """
        Enters the context manager
        setting sys.tracebacklimit to temp_limit
        """
        sys.tracebacklimit = self.temp_limit

    def __exit__(self, *args, **kwargs):
        """
        Exits the context manager
        settings sys.tracebacklimit to predefined_limit
        """
        sys.tracebacklimit = self.predefined_limit
