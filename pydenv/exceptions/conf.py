"""
Configuration for the pydenv.exception package
"""
import os
from pydenv.conf import PyDenvObject
from pydenv.output import TextStyles


class PyDenvExceptionConfig(PyDenvObject):
    """
    PyDenv Exception configuration
    Allows the user to personalise the behaviour of PyDenv
    """
    PYDENV_EXCEPTION_TEMPLATE = "{exc_class}: {instance}\n{traces}"
    PYDENV_TRACEBACK_TEMPLATE = "- File {filename}:{lineno} in {name}:\n    {line}"
    PYDENV_EXCEPTION_PRINT_FUNCTIONS = {
        "print": {
            "func": print,
            "apply_tags": True,
            "apply_styles": True,
        },
        "info": {
            "func": PyDenvObject.DEFAULT_LOGGER.info,
            "apply_tags": False,
            "apply_styles": False,
        },
    }
    PYDENV_EXCEPTION_TAGS = {
        "venv": {
            "repl": "[venv]",
            "value": os.environ.get("VIRTUAL_ENV", None),
        },
        "base_dir": {
            "repl": "[base_dir]",
            "value": os.environ.get("PWD", None),
            "style": TextStyles.colors.foreground.RED,
        },
    }


class PyDenvDjangoExceptionConfig(PyDenvExceptionConfig):
    # Set to 0 to disable traceback lines
    # from the default django exception handler
    PYDENV_DJANGO_TRACEBACK_LINES = 1
    # Number of traceback lines in the pydenv output
    PYDENV_TRACEBACK_LINES = 20
