

class PyDenvException(Exception):
    pass


class PyDenvInvalidTraceback(PyDenvException):
    pass

class PyDenvImproperlyConfigured(PyDenvException):
    pass
