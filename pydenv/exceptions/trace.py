"""
Trace module
"""
from traceback import print_stack
from pydenv.exceptions.conf import PyDenvExceptionConfig
from pydenv.exceptions.internal import PyDenvImproperlyConfigured
from pydenv.output import TextStyles
from pydenv.decorators import cached_property


class PyDenvTrace(PyDenvExceptionConfig):
    """
    Generalisation of the trace line in a traceback
    """

    def __init__(self, trace):
        """
        Inits the PyDenvTrace object
        :param trace: element from traceback.extract_tb or traceback.extract_stack
        """
        self.trace = trace

    @cached_property
    def template(self):
        """
        Returns the template of the trace element
        :returns: str
        """
        return self.PYDENV_TRACEBACK_TEMPLATE.format(
            filename=self.trace.filename,
            lineno=self.trace.lineno,
            name=self.trace.name or "<No name argument provided>",
            line=self.trace.line or "<No line argument provided>",
        )

    @cached_property
    def spec(self):
        """
        Reads the tags in the template and returns them in a dict
        Tags are defined in PyDenvExceptionConf
        :raises: ValueError - If it can't read the template
        :returns: dict
        """
        template = self.template
        spec = {
            "tags": [],
            "styles": [],
        }

        if template is None:
            raise PyDenvImproperlyConfigured("Could not read tags.")

        for key in self.PYDENV_EXCEPTION_TAGS:
            tag = self.PYDENV_EXCEPTION_TAGS[key]
            if tag["value"] and tag["value"] in template:
                spec["tags"].append(tag)
                spec["styles"].append(tag.get("style", TextStyles.colors.foreground.BRIGHT_BLACK))

        return spec

    def format(self, apply_tags=False, apply_styles=False):
        """
        Formats the template, applying tags and styles if required
        :param apply_tags: Runs tags substitutions
        :param apply_styles: Applies terminal styles to the output
        :returns: str
        """
        template = self.template
        spec = self.spec
        if apply_tags:
            for tag in spec["tags"]:
                template = template.replace(tag["value"], tag["repl"])

        if apply_styles:
            template = TextStyles.format_text(template, styles=spec["styles"])

        return template

    def __str__(self):
        """
        String representation of the trace
        :returns: str
        """
        return self.format()

    def __repr__(self):
        """
        Repr representation of the trace
        :returns: str
        """
        return "<{cls_name}: {trace}>".format(
            cls_name=self.__class__.__name__,
            trace=self.trace
        )
