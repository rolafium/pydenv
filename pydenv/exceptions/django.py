"""
Exception middleware handlers for Django
"""
import sys
from pydenv.exceptions.conf import PyDenvDjangoExceptionConfig
from pydenv.exceptions.context_managers import TempTracebackLimit
from pydenv.exceptions.functions import (
    pydenv_extract_tb,
    pydenv_get_trace_list,
    pydenv_report_exception,
)


class PyDenvExceptionMiddleware(PyDenvDjangoExceptionConfig):
    """
    Middleware to be passed to django
    """
    def __init__(self, get_response):
        """Middleware init function"""
        self.get_response = get_response
        sys.tracebacklimit = self.PYDENV_DJANGO_TRACEBACK_LINES

    def __call__(self, request):
        """Middleware call"""
        response = self.get_response(request)
        return response

    def process_exception(self, request, exception):
        """
        Processes the exception raised in the request/response cycle
        It will report the exception as defined in PyDenvExceptionConfig
        """
        with TempTracebackLimit(temp_limit=self.PYDENV_TRACEBACK_LINES):
            tb = pydenv_extract_tb(exception)
            pydenv_report_exception(exception, tb)
            # Return None so django can process the exception
            # with exception handlers defined in other middlewares
            # including the django default exception handler
            return None
