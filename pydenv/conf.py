"""
Configuration object for PyDenv
"""
import logging

LOGGER = logging.getLogger(__name__)


class PyDenvObject(object):
    """PyDenvObject from which all PyDenv class inherit"""
    DEFAULT_LOGGER = LOGGER

    @classmethod
    def is_debug(cls):
        """Returns whether we are in a debug environment"""
        return False


    @classmethod
    def set_is_debug_func(cls, func):
        """
        Sets a function that will be used to determinate
        if we are in a debug environment
        :param func: function (accepts 1 parameter -> the PyDenvObject class)
        """
        cls.is_debug = classmethod(func)


    @classmethod
    def activate_debug(cls):
        """Activates the debug env by passing a function that returns True"""
        cls.set_is_debug_func(lambda obj: True)

    @classmethod
    def deactivate_debug(cls):
        """Deactivates the debug env by passing a function that returns False"""
        cls.set_is_debug_func(lambda obj: False)


# Set the original is_debug method in __isdebug__
# so it can be restored
PyDenvObject.__isdebug__ = PyDenvObject.is_debug
