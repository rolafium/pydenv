"""
PyDenv decorators
"""

def cached_property(func):
    """
    Cached property decorator
    Returns the property value as defined in the getter
    however, if the property was already evaluated once for the object
    returns the first evaluation instead
    :param: func
    :returns: func
    """
    internal_attr_name = "_pydenv_cached_" + func.__name__

    def wrapped_func(obj):
        """Wrapped function"""
        if not hasattr(obj, internal_attr_name):
            attr_value = func(obj)
            setattr(obj, internal_attr_name, attr_value)
        return getattr(obj, internal_attr_name)

    return property(wrapped_func)
