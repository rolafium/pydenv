"""
Dev utilities for the output text
"""


class StylesCodes:
    """Style codes"""
    DEFAULT = "0"
    BOLD = "1"
    FAINT = "2"
    ITALIC = "3"
    UNDERLINE = "4"


class ForegroundColorsCodes:
    """Foreground color codes"""
    BLACK = "30"
    RED = "31"
    GREEN = "32"
    YELLOW = "33"
    BRIGHT_BLACK = "30;1"
    WHITE = "37"

class BackgroundColorsCodes:
    """Background color codes"""
    BLACK = "40"
    RED = "41"
    GREEN = "42"
    YELLOW = "43"
    BRIGHT_BLACK = "100"
    WHITE = "47"

class ColorsCodes:
    """Wrapper class for color codes"""
    foreground = ForegroundColorsCodes
    background = BackgroundColorsCodes


class TextStyles:
    """Text Styles utilities"""
    CODE_TEMPLATE = "\033[{codes}m"
    DEFAULT_FORMATTED = "\033[0m"
    styles = StylesCodes
    colors = ColorsCodes
    

    @classmethod
    def format_text(cls, text, styles=[]):
        """
        Formats the text
        :param text: str
        :param styles: list of styles
        :returns: str
        """
        if not styles:
            return text

        start_code = cls.CODE_TEMPLATE.format(codes=";".join(styles))
        end_code = cls.DEFAULT_FORMATTED
        return start_code + text + end_code
